package com.gurudwara.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class SquareView extends LinearLayout{

	public SquareView(Context context) {
		super(context);
	}

	
	public SquareView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	
	public void onLayout(boolean changed, int l, int t, int r, int b) {
		int width = r - l;
		ViewGroup.LayoutParams params = this.getLayoutParams();
		params.height = width;
		this.setLayoutParams(params);
		this.setMeasuredDimension(width, width);
		super.onLayout(changed, l, t, r, t + width);
	}


	public SquareView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	

}
