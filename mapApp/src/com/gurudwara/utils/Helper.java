package com.gurudwara.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.widget.TextView;

public class Helper {
	
	public static void setFont(Context context,TextView v)
	{
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "anmolunib.ttf");
		Log.e("", "type facec is : "+tf);
		v.setTypeface(tf);
	}
	
}
