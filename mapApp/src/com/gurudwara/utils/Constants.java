package com.gurudwara.utils;

import java.util.ArrayList;

import android.graphics.Typeface;

import com.android.GurudwaraApplication.R;

public class Constants {

	private static final String baseUrl="http://archive.jagbani.com/admincontrol/xml/";
	public static final String historyUrl=baseUrl+"gurudwara-history.json";
	public static final String eventUrl=baseUrl+"gurudwara-event.json";
	public static final String newsUrl=baseUrl+"gurudwaranews.json";
	public static final String nameUrl=baseUrl+"allgurudwara.json";
	public static final String photoGalleryUrl=baseUrl+"gurudwaragallery.json";
	public static final String historyDetails="dtls";
	public static final String eventHeading="head_line";
	public static final String eventDescription="dtls";
	public static final String eventDate="eventDate";
	public static final String eventImage="img_name";
	public static ArrayList<Integer> image_store = new ArrayList<Integer>();
	public static final String gurudwaraId="gurudwaraId";
	public static final String newsId="news_id";
	public static final String newsDate="newsDate";
	public static final String latitude="Latitude";
	public static final String longitude="Longitude";
	public static final String gurudwaraAddress="address";
	public static String mainId = "";
	public static final String gurudwaraName="gurudwaraname";
	public static final String gurudwaraCity="city";
	public static final String gurudwaraState="state";
	public static final String historyGurudwaraName="gurudwara_name";
	public static final String historyGurudwaraAddress="gurudwara_address";
	public static int screenHeight;
	public static int screenWidth;
	
	public static void setTypeFace()
	{
		
	}
}
