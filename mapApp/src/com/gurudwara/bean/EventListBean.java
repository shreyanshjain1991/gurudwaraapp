package com.gurudwara.bean;

import java.util.ArrayList;

public class EventListBean {

	private String eventHeading;
	private String eventDescription;
	private ArrayList<String> eventList;
	private String eventDate;
	private String urlEventImage;
	private String eventId;
	
	
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getUrlEventImage() {
		return urlEventImage;
	}
	public void setUrlEventImage(String urlEventImage) {
		this.urlEventImage = urlEventImage;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public ArrayList<String> getEventList() {
		return eventList;
	}
	public void setEventList(ArrayList<String> eventList) {
		this.eventList = eventList;
	}
	public String getEventHeading() {
		return eventHeading;
	}
	public void setEventHeading(String eventHeading) {
		this.eventHeading = eventHeading;
	}
	public String getEventDescription() {
		return eventDescription;
	}
	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}
	
	
}
