package com.gurudwara.bean;

public class GurudwaraNameBean {

	private String gurudwaraName;
	private String gurudwaraAddress;
	private String gurudwaraLat;
	private String gurudwaraLon;
	private String gurudwaraCity;
	private String gurudwaraState;
	private String gurudwaraId;
	
	
	public String getGurudwaraId() {
		return gurudwaraId;
	}
	public void setGurudwaraId(String gurudwaraId) {
		this.gurudwaraId = gurudwaraId;
	}
	public String getGurudwaraCity() {
		return gurudwaraCity;
	}
	public void setGurudwaraCity(String gurudwaraCity) {
		this.gurudwaraCity = gurudwaraCity;
	}
	public String getGurudwaraState() {
		return gurudwaraState;
	}
	public void setGurudwaraState(String gurudwaraState) {
		this.gurudwaraState = gurudwaraState;
	}
	public String getGurudwaraLat() {
		return gurudwaraLat;
	}
	public void setGurudwaraLat(String gurudwaraLat) {
		this.gurudwaraLat = gurudwaraLat;
	}
	public String getGurudwaraLon() {
		return gurudwaraLon;
	}
	public void setGurudwaraLon(String gurudwaraLon) {
		this.gurudwaraLon = gurudwaraLon;
	}
	public String getGurudwaraName() {
		return gurudwaraName;
	}
	public void setGurudwaraName(String gurudwaraName) {
		this.gurudwaraName = gurudwaraName;
	}
	public String getGurudwaraAddress() {
		return gurudwaraAddress;
	}
	public void setGurudwaraAddress(String gurudwaraAddress) {
		this.gurudwaraAddress = gurudwaraAddress;
	}

	
	
}
