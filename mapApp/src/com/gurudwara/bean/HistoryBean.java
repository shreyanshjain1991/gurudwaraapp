package com.gurudwara.bean;

public class HistoryBean {

	private String historyDetails;
	private String historyGurudwaraName;
	private String historyGurudwaraAddress;
	private String historyImageUrl;
	public String getHistoryDetails() {
		return historyDetails;
	}
	public void setHistoryDetails(String historyDetails) {
		this.historyDetails = historyDetails;
	}
	public String getHistoryGurudwaraName() {
		return historyGurudwaraName;
	}
	public void setHistoryGurudwaraName(String historyGurudwaraName) {
		this.historyGurudwaraName = historyGurudwaraName;
	}
	public String getHistoryGurudwaraAddress() {
		return historyGurudwaraAddress;
	}
	public void setHistoryGurudwaraAddress(String historyGurudwaraAddress) {
		this.historyGurudwaraAddress = historyGurudwaraAddress;
	}
	
	public String getHistoryImageUrl() {
		return historyImageUrl;
	}
	public void setHistoryImageUrl(String historyImageUrl) {
		this.historyImageUrl = historyImageUrl;
	}
	
	
	
	
}
