package com.gurudwara.bean;

import java.util.ArrayList;

public class PhotoGalleryBean {

	public ArrayList<String> imageUrls;
	
	public PhotoGalleryBean() {
		imageUrls = new ArrayList<String>();
	}

	public ArrayList<String> getImageUrls() {
		return imageUrls;
	}

	public void setImageUrls(ArrayList<String> imageUrls) {
		this.imageUrls = imageUrls;
	}
	
}
