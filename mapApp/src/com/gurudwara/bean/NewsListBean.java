package com.gurudwara.bean;

import java.util.ArrayList;

public class NewsListBean {

	private String newsId;
	private String newsHeading;
	private String newsDescription;
	private ArrayList<String> newsList;
	private String newsDate;
	private String urlNewsImage;
	
	
	
	public String getNewsId() {
		return newsId;
	}
	public void setNewsId(String newsId) {
		this.newsId = newsId;
	}
	public String getNewsHeading() {
		return newsHeading;
	}
	public void setNewsHeading(String newsHeading) {
		this.newsHeading = newsHeading;
	}
	public String getNewsDescription() {
		return newsDescription;
	}
	public void setNewsDescription(String newsDescription) {
		this.newsDescription = newsDescription;
	}
	public ArrayList<String> getNewsList() {
		return newsList;
	}
	public void setNewsList(ArrayList<String> newsList) {
		this.newsList = newsList;
	}
	public String getNewsDate() {
		return newsDate;
	}
	public void setNewsDate(String newsDate) {
		this.newsDate = newsDate;
	}
	public String getUrlNewsImage() {
		return urlNewsImage;
	}
	public void setUrlNewsImage(String urlNewsImage) {
		this.urlNewsImage = urlNewsImage;
	}
	
	
}
