package com.gurudwara.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.gurudwara.bean.EventListBean;
import com.gurudwara.bean.GurudwaraNameBean;
import com.gurudwara.bean.HistoryBean;
import com.gurudwara.bean.NewsListBean;
import com.gurudwara.bean.PhotoGalleryBean;
import com.gurudwara.utils.Constants;

public class JsonParser {

	public static HistoryBean parseJsonRespHistory(String resp){
		HistoryBean historyBean=new HistoryBean();
		try {			
			JSONArray array = new JSONArray(resp);

			for(int i=0;i<array.length();i++)
			{

				JSONObject obj = array.getJSONObject(0);
				if(obj.getString(Constants.gurudwaraId).equals(Constants.mainId));
				{
					historyBean.setHistoryGurudwaraName(obj.getString(Constants.historyGurudwaraName));
					historyBean.setHistoryGurudwaraAddress(obj.getString(Constants.historyGurudwaraAddress));
					historyBean.setHistoryImageUrl(obj.getString(Constants.eventImage));
					historyBean.setHistoryDetails(obj.getString(Constants.historyDetails));
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return historyBean;
	}

	public static ArrayList<EventListBean> parseJsonRespEvent(String resp)
	{
		ArrayList<EventListBean> eventBeanList=new ArrayList<EventListBean>();
		try {			
			JSONArray array = new JSONArray(resp);

			for(int i=0;i<array.length();i++)
			{
				EventListBean eventBean=new EventListBean();
				JSONObject obj = array.getJSONObject(i);
				Log.e("", "main id: "+Constants.mainId);
				if(obj.getString(Constants.gurudwaraId).equals(Constants.mainId))
				{
					Log.e("", "main id: "+Constants.mainId);
					eventBean.setEventHeading(obj.getString(Constants.eventHeading));
					eventBean.setEventDescription(obj.getString(Constants.eventDescription));
					eventBean.setEventDate(obj.getString(Constants.eventDate));
					eventBean.setUrlEventImage(obj.getString(Constants.eventImage));
					eventBeanList.add(eventBean);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return eventBeanList;
	}
	
	
	
	
	
	public static ArrayList<NewsListBean> parseJsonRespNews(String resp)
	{
		ArrayList<NewsListBean> newsBeanList=new ArrayList<NewsListBean>();
		try {			
			JSONArray array = new JSONArray(resp);

			for(int i=0;i<array.length();i++)
			{
				NewsListBean newsBean=new NewsListBean();
				JSONObject obj = array.getJSONObject(i);
				
				if(obj.getString(Constants.gurudwaraId).equals(Constants.mainId))
				{
					newsBean.setNewsHeading(obj.getString(Constants.eventHeading));
					newsBean.setNewsDescription(obj.getString(Constants.eventDescription));
					newsBean.setNewsDate(obj.getString(Constants.newsDate));
					newsBean.setUrlNewsImage(obj.getString(Constants.eventImage));
					newsBeanList.add(newsBean);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return newsBeanList;
	}

	public static GurudwaraNameBean parseJsonRespMain(String resp)
	{
		GurudwaraNameBean nameBean=new GurudwaraNameBean();
		try{
			JSONArray array= new JSONArray(resp);
			for(int i=0;i<array.length();i++)
			{
				JSONObject obj=array.getJSONObject(i);
				
				if(obj.getString(Constants.gurudwaraId).equals("1"))
				{
					Constants.mainId=obj.getString(Constants.gurudwaraId);
					Log.e("", "gurudwara name bean main id: "+Constants.mainId);
					nameBean.setGurudwaraName(obj.getString(Constants.gurudwaraName));
					nameBean.setGurudwaraAddress(obj.getString(Constants.gurudwaraAddress));
					nameBean.setGurudwaraLat(obj.getString(Constants.latitude));
					nameBean.setGurudwaraLon(obj.getString(Constants.longitude));
					nameBean.setGurudwaraState(obj.getString(Constants.gurudwaraState));
					nameBean.setGurudwaraCity(obj.getString(Constants.gurudwaraCity));
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return nameBean;
	}
	
	public static ArrayList<String> parseJsonRespGallery(String resp)
	{
		ArrayList<String> imageUrls=new ArrayList<String>();
		try{
			JSONArray array=new JSONArray(resp);
			for(int i=0;i<array.length();i++)
			{
				JSONObject obj=array.getJSONObject(i);
				if(obj.getString(Constants.gurudwaraId).equals(Constants.mainId))
				{
					imageUrls.add(obj.getString(Constants.eventImage));
					
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return imageUrls;
	}
}
