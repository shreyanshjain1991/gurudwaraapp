package com.gurudwara.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.GurudwaraApplication.R;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

public class AdapterPhotoGallery extends BaseAdapter {

	private ArrayList<String> images;
	private Context con;
	private LayoutInflater myInflater;
	static int counter = 0;

	public AdapterPhotoGallery(Context context, ArrayList<String> images) {
		AdapterPhotoGallery.counter = 0;
		this.images = images;
		con=context;
		myInflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return images.size();
	}

	@Override
	public Object getItem(int arg0) {
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View arg1, ViewGroup arg2) {
		ViewHolder holder;

		if(arg1==null)
		{
			arg1=(LinearLayout)myInflater.inflate(R.layout.list_item_gallery,null);
			holder=new ViewHolder();
			holder.singleItem=(ImageView)arg1.findViewById(R.id.ivSingle);
			arg1.setTag(holder);
		}else{
			holder=(ViewHolder)arg1.getTag();
		}

		Log.e("counter = "+AdapterPhotoGallery.counter, " URL : "+images.get(position));


		UrlImageViewHelper.setUrlDrawable(holder.singleItem, images.get(position), R.drawable.placeholder);
		AdapterPhotoGallery.counter++;
		return arg1;
	}

	public class ViewHolder{
		ImageView singleItem;
	}

}


