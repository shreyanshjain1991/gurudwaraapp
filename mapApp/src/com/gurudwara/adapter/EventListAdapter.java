package com.gurudwara.adapter;



import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.GurudwaraApplication.MainActivity;
import com.android.GurudwaraApplication.R;
import com.gurudwara.bean.EventListBean;
import com.gurudwara.utils.Helper;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

public class EventListAdapter extends BaseAdapter {

	Context context;
	ArrayList<EventListBean> eventBean;
	LayoutInflater inflater;
	String eventDate;
	private LinearLayout.LayoutParams margins;
	
	public EventListAdapter(Context context,ArrayList<EventListBean> bean) {
		this.context=context;
		inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		eventBean = new ArrayList<EventListBean>();
		eventBean.clear();
		eventBean.addAll(bean);
		
	}


	
	
	@Override
	public int getCount() {
		return eventBean.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView==null)
		{
			holder=new ViewHolder();
			convertView = (View)inflater.inflate(R.layout.event_single, null);
			holder.eventTitle=(TextView)convertView.findViewById(R.id.tv_event_heading);
			holder.eventDate=(TextView)convertView.findViewById(R.id.tv_event_date);
			holder.eventImage=(ImageView)convertView.findViewById(R.id.iv_event_image);
			holder.eventJoin=(ImageView)convertView.findViewById(R.id.iv_join_event);
			
			/**
			 * id = 10
			 * adpater event join on click listener 
			 */
//			holder.eventJoin.setOnClickListener(new OnClickListener() {
//				
//				@Override
//				public void onClick(View arg0) {
//					Toast.makeText(context, "Button clicked", 2000).show();
//				}
//			});
			holder.eventDivider=(TextView)convertView.findViewById(R.id.tv_event_divider);
			holder.event_single_layout=(LinearLayout)convertView.findViewById(R.id.event_single_layout);
			convertView.setTag(holder);
		}else{
			holder=(ViewHolder) convertView.getTag();	
		}

		//adjustLayout( holder);
		holder.eventTitle.setText(Html.fromHtml(eventBean.get(position).getEventHeading().toString()).toString());
		Helper.setFont(context, holder.eventTitle);
		Log.e("", "EventListAdapter heading: "+eventBean.get(position).getEventHeading().toString());
		holder.eventTitle.setTextColor(context.getResources().getColor(R.color.app_orange));
		
		long l=Long.parseLong(eventBean.get(position).getEventDate().toString().substring(6,eventBean.get(position).getEventDate().toString().length()-2));
		SimpleDateFormat formatter=new SimpleDateFormat();
		eventDate=formatter.format(l);
		holder.eventDate.setText(eventDate);
		
		UrlImageViewHelper.setUrlDrawable(holder.eventImage, eventBean.get(position).getUrlEventImage());
		holder.eventImage.setScaleType(ScaleType.FIT_XY);
		return convertView;	
	}
}

class ViewHolder{
	public TextView eventTitle;
	public TextView eventDate;
	public ImageView eventImage;
	public ImageView eventJoin;
	public TextView eventDivider;
	public LinearLayout event_single_layout;
}