package com.gurudwara.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.android.GurudwaraApplication.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MainAdapter extends BaseAdapter{

	private String currentMonth;
	private String currentDate;
	private TextView tv_monthEvent;
	private TextView tv_dateEvent;
	private ArrayList<View> mainLayouts;

	public MainAdapter(Context context, ArrayList<View> mainLayouts) {

		this.mainLayouts=mainLayouts;
		Log.e("Size of menu arraylist is ", "size : "+this.mainLayouts.size()+"");
		tv_monthEvent=(TextView)mainLayouts.get(3).findViewById(R.id.tv_monthEvent);
		tv_dateEvent=(TextView)mainLayouts.get(3).findViewById(R.id.tv_dateEvent);
	}

	@Override
	public int getCount() {
		return mainLayouts.size();
	}

	@Override
	public Object getItem(int arg0) {
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	
		if(position==3)
			getCurrentDate();
		return mainLayouts.get(position);
	}

	public void getCurrentDate()
	{
		Calendar calendar=Calendar.getInstance();
		Date date = calendar.getTime();
		SimpleDateFormat format = new SimpleDateFormat("MMM");
		currentMonth = format.format(date);
		SimpleDateFormat format1 = new SimpleDateFormat("dd");
		currentDate = format1.format(date);
		tv_monthEvent.setText(currentMonth);
		tv_dateEvent.setText(currentDate);
	}
	
}
