package com.gurudwara.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

import com.android.GurudwaraApplication.MainActivity;
import com.android.GurudwaraApplication.R;
import com.gurudwara.bean.EventListBean;
import com.gurudwara.bean.NewsListBean;
import com.gurudwara.utils.Helper;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

public class NewsListAdapter extends BaseAdapter{
	Context context;
	ArrayList<NewsListBean> newsBean;
	LayoutInflater inflater;
	String newsDate;
	private LinearLayout.LayoutParams margins;
	
	public NewsListAdapter(Context context,ArrayList<NewsListBean> bean) {
		this.context=context;
		inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		newsBean = new ArrayList<NewsListBean>();
		newsBean.clear();
		newsBean.addAll(bean);
		
	}

	
	@Override
	public int getCount() {
		return newsBean.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		newsViewHolder holder;
		if(convertView==null)
		{
			holder=new newsViewHolder();
			convertView = (View)inflater.inflate(R.layout.news_single, null);
			holder.newsHeading=(TextView)convertView.findViewById(R.id.tv_news_heading);
			holder.newsDate=(TextView)convertView.findViewById(R.id.tv_news_date);
			holder.newsImage=(ImageView)convertView.findViewById(R.id.iv_news_image);
			
			holder.news_single_layout=(LinearLayout)convertView.findViewById(R.id.news_single_layout);
			convertView.setTag(holder);
		}else{
			holder=(newsViewHolder) convertView.getTag();	
		}

		//adjustLayout( holder);
		holder.newsHeading.setText(newsBean.get(position).getNewsHeading().toString());
		Helper.setFont(context,holder.newsHeading);
		Log.e("", "NewsListAdapter heading: "+newsBean.get(position).getNewsHeading().toString());
		holder.newsHeading.setTextColor(context.getResources().getColor(R.color.app_orange));
		
		long l=Long.parseLong(newsBean.get(position).getNewsDate().toString().substring(6,newsBean.get(position).getNewsDate().toString().length()-2));
		SimpleDateFormat formatter=new SimpleDateFormat();
		newsDate=formatter.format(l);
		holder.newsDate.setText(newsDate);
		
		UrlImageViewHelper.setUrlDrawable(holder.newsImage, newsBean.get(position).getUrlNewsImage());
		holder.newsImage.setScaleType(ScaleType.FIT_XY);
		return convertView;	
	}
}

class newsViewHolder{
	public TextView newsHeading;
	public TextView newsDate;
	public ImageView newsImage;
	public LinearLayout news_single_layout;
}

