package com.android.GurudwaraApplication;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.gurudwara.adapter.MainAdapter;
import com.gurudwara.bean.GurudwaraNameBean;
import com.gurudwara.parser.JsonParser;
import com.gurudwara.utils.ConnectionDetector;
import com.gurudwara.utils.Constants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
public class MainActivity extends Activity{


	private int oldy=0;
	private float alpha;

	private TextView tv_gurudwara_name_swipe_single;
	private TextView tv_loc_swipe_single;
	private TextView tv_sub_loc_swipe_single;
	private TextView tv_name_map_single;
	private TextView tv_loc_map_single;
	private TextView tv_sub_loc_map_single;
	private TextView tv_name_news_single;
	private TextView tv_refresh_swipe_single;


	private LinearLayout layout_tvs_swipe;
	private LinearLayout layout_iv_swipe;
	private LinearLayout layout_tvs_map;
	private LinearLayout layout_iv_map;
	private LinearLayout layout_tvs_news;
	private LinearLayout layout_iv_news;

	private ActionBar homeBar;
	public static Typeface tf;
	private String lat;
	private String lon;
	private String gurudwaraName;
	private String gurudwaraLocation;

	private ProgressBar progressBarMain;
	private ProgressBar progressBarMap;
	private ProgressBar progressBarNews;
	private ConnectionDetector checkConnection;

	private ArrayList<View> mainLayouts;
	private ListView lvMain;
	private Context context;
	private MainAdapter adapter;

	private ImageView iv_main;
	private Button b_refresh_swipe;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initialiseVariable();
		addToMainLayouts();
		checkingInternetConnections();
		refreshButtonFunction();
		lvMain.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long arg3) {
				if(position==0)
				{}else
				{
					if(position==3){
						if(checkConnectionMethod())
						{
							Intent i=new Intent(MainActivity.this,EventListActivity.class);
							startActivity(i);
						}
					}else if(position==2){
						if(checkConnectionMethod())
						{
							Intent i=new Intent(MainActivity.this,NewsActivity.class);

							startActivity(i);
						}
					}
					else if(position==4){
						if(checkConnectionMethod())
						{
							Intent i=new Intent(MainActivity.this,HistoryActivity.class);
							startActivity(i);
						}
					}
					else if(position==1){
						if(checkConnectionMethod())
						{
							if(progressBarMap.getVisibility()==View.INVISIBLE)
							{
								Intent i=new Intent(MainActivity.this,MapActivity.class);
								i.putExtra("gurudwaraName", gurudwaraName);
								i.putExtra("gurudwaraLocation", gurudwaraLocation);
								i.putExtra("lat", lat);
								i.putExtra("lon", lon);
								startActivity(i);
							}
						}
					}
					else if(position==5){
						if(checkConnectionMethod())
						{
							Intent i=new Intent(MainActivity.this,PhotoGalleryActivity.class);
							startActivity(i);
						}
					}
				}
			}
		});
		calculateScreenSize();

	}

	@SuppressLint("NewApi")
	public void addToMainLayouts()
	{
		mainLayouts=new ArrayList<View>();
		LayoutInflater inflater=(LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View item_swipe_view = (View)inflater.inflate(R.layout.single_item_swipe, null);
		progressBarMain=(ProgressBar)item_swipe_view.findViewById(R.id.progress_bar_main_single);
		layout_tvs_swipe=(LinearLayout)item_swipe_view.findViewById(R.id.layout_tvs_swipe);
		layout_iv_swipe=(LinearLayout)item_swipe_view.findViewById(R.id.layout_iv_swipe);
		tv_gurudwara_name_swipe_single=(TextView)item_swipe_view.findViewById(R.id.tv_name_swipe_single);
		tv_loc_swipe_single=(TextView)item_swipe_view.findViewById(R.id.tv_loc_swipe_single);
		tv_sub_loc_swipe_single=(TextView)item_swipe_view.findViewById(R.id.tv_Sub_loc_swipe_single);
		tv_refresh_swipe_single=(TextView)item_swipe_view.findViewById(R.id.tv_refresh_swipe_single);
		b_refresh_swipe=(Button)item_swipe_view.findViewById(R.id.b_refresh_swipe_single);
		mainLayouts.add(item_swipe_view);

		View item_map_view = (View)inflater.inflate(R.layout.single_item_map, null);
		progressBarMap=(ProgressBar)item_map_view.findViewById(R.id.progress_bar_map_single);
		layout_tvs_map=(LinearLayout)item_map_view.findViewById(R.id.layout_tvs_map);
		layout_iv_map=(LinearLayout)item_map_view.findViewById(R.id.layout_iv_map);
		tv_name_map_single=(TextView)item_map_view.findViewById(R.id.tv_name_map_single);
		tv_loc_map_single=(TextView)item_map_view.findViewById(R.id.tv_loc_map_single);
		tv_sub_loc_map_single=(TextView)item_map_view.findViewById(R.id.tv_Sub_loc_map_single);
		mainLayouts.add(item_map_view);

		View item_news_view = (View)inflater.inflate(R.layout.single_item_news, null);
		progressBarNews=(ProgressBar)item_news_view.findViewById(R.id.progress_bar_news_single);
		layout_tvs_news=(LinearLayout)item_news_view.findViewById(R.id.layout_tvs_news);
		layout_iv_news=(LinearLayout)item_news_view.findViewById(R.id.layout_iv_news);
		tv_name_news_single=(TextView)item_news_view.findViewById(R.id.tv_name_news_single);
		mainLayouts.add(item_news_view);


		View item_event_view = (View)inflater.inflate(R.layout.single_item_event, null);

		mainLayouts.add(item_event_view);

		View item_history_view = (View)inflater.inflate(R.layout.single_item_history, null);
		mainLayouts.add(item_history_view);

		View item_gallery_view = (View)inflater.inflate(R.layout.single_item_gallery, null);
		mainLayouts.add(item_gallery_view);


	}


public void refreshButtonFunction()
{
	if(checkConnectionMethod())
	{
		b_refresh_swipe.setVisibility(View.GONE);
		tv_refresh_swipe_single.setVisibility(View.GONE);
	}else
	{
		b_refresh_swipe.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if(checkConnectionMethod())
					{
					b_refresh_swipe.setVisibility(View.GONE);
					tv_refresh_swipe_single.setVisibility(View.GONE);
					setBarVisible();
					setLayoutsInvisible();
					checkingInternetConnections();
					}else
						setBarInvisible();
						setLayoutsInvisible();
			}
		});
		setBarInvisible();
		setLayoutsInvisible();
	}
}

	public void changeBackgroundImage()
	{
		View v=adapter.getView(0, null, lvMain);
		View v2=adapter.getView(1, null, lvMain);
		if(v!=null && v2!=null)
		{

			int top=-v.getTop();
			Log.e("", "top: "+top);


			alpha=(float) top/(float)Constants.screenHeight;
			Log.e("", "alpha: "+(int) (255*alpha));
			if(alpha<=1)
			{
				iv_main.getBackground().setAlpha((int) (255*alpha));			
			}
			else
				iv_main.getBackground().setAlpha(255);



		}
	}


	public void initialiseVariable()
	{
		checkConnection=new ConnectionDetector(MainActivity.this);
		lvMain=(ListView)findViewById(R.id.lvMain);
		iv_main=(ImageView)findViewById(R.id.iv_main);
		iv_main.setBackgroundResource(R.drawable.bg_real_blur_5);
		iv_main.getBackground().setAlpha(0);

		context=MainActivity.this;
	}

	public void checkingInternetConnections()
	{
		if(checkConnectionMethod()){
			getDataFromUrl(Constants.nameUrl);
		}else
		{
			setMainAdapter();
			setLayoutsInvisible();
		}
	}

	public boolean checkConnectionMethod()
	{
		if(checkConnection.isConnectingToInternet()){
			return true;
		}else{

			Toast.makeText(MainActivity.this, MainActivity.this.getResources().getString(R.string.please_connect_to_internet), Toast.LENGTH_LONG).show();
			return false;
		}	
	}

	public void homeBarFormatting()
	{
		homeBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ee5c0c")));
		homeBar.setDisplayShowHomeEnabled(false);
		homeBar.setDisplayShowTitleEnabled(false);
		homeBar.hide();
	}





	//id=12
	//adjust the screen margins.

	public void calculateScreenSize() {

		Rect rect = new Rect(); 
		Window win = (Window) getWindow(); 
		win.getDecorView().getWindowVisibleDisplayFrame(rect); 
		Constants.screenHeight = rect.height();
		Constants.screenWidth= rect.width();
	}

	public void getDataFromUrl(String webUrl)
	{
		RequestParams params=new RequestParams();
		AsyncHttpClient client=new AsyncHttpClient();
		client.get(webUrl, params,new AsyncHttpResponseHandler()
		{
			@Override
			public void onFailure(Throwable arg0, String arg1) {
				Log.d("onFailure","in on failure "+arg1);
				super.onFailure(arg0, arg1);
			}

			@Override
			public void onFinish() {
				Log.d("onFinish","in on finish ");
				super.onFinish();
			}

			@Override
			public void onStart() {
				super.onStart();
			}



			@Override
			public void onSuccess(String arg0) {
				Log.d("onSuccess","the string : "+arg0);
				GurudwaraNameBean bean =  JsonParser.parseJsonRespMain(arg0);
				if (bean!=null) {
					setBarInvisible();
					setLayoutVISIBLE();
					setTextToViews(bean);
					setMainAdapter();




					lvMain.setOnScrollListener(new OnScrollListener() {
						@Override
						public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
							changeBackgroundImage();
						}

						@Override
						public void onScrollStateChanged(AbsListView arg0,
								int arg1) {
						}
					});
				}
				super.onSuccess(arg0);
			}


		}
				);


	}
	private void setLayoutsInvisible() {
		layout_tvs_swipe.setVisibility(View.INVISIBLE);
		layout_iv_swipe.setVisibility(View.INVISIBLE);
		layout_tvs_map.setVisibility(View.INVISIBLE);
		layout_iv_map.setVisibility(View.INVISIBLE);
		layout_tvs_news.setVisibility(View.INVISIBLE);
		layout_iv_news.setVisibility(View.INVISIBLE);
	}

	private void setTextToViews(GurudwaraNameBean bean) {
		tv_gurudwara_name_swipe_single.setText(bean.getGurudwaraName());
		tv_loc_swipe_single.setText(bean.getGurudwaraAddress());
		tv_sub_loc_swipe_single.setText(bean.getGurudwaraCity()+", "+bean.getGurudwaraState());
		tv_name_map_single.setText(bean.getGurudwaraName());
		tv_loc_map_single.setText(bean.getGurudwaraAddress());
		tv_sub_loc_map_single.setText(bean.getGurudwaraCity()+", "+bean.getGurudwaraState());
		tv_name_news_single.setText(bean.getGurudwaraName());
		gurudwaraName=bean.getGurudwaraName();
		gurudwaraLocation=bean.getGurudwaraAddress();
		lat=bean.getGurudwaraLat();
		lon=bean.getGurudwaraLon();
	}

	private void setBarInvisible() {
		progressBarMain.setVisibility(View.INVISIBLE);
		progressBarMap.setVisibility(View.INVISIBLE);
		progressBarNews.setVisibility(View.INVISIBLE);
	}
	
	private void setLayoutVISIBLE()
	{
		layout_tvs_swipe.setVisibility(View.VISIBLE);
		layout_iv_swipe.setVisibility(View.VISIBLE);
		layout_tvs_map.setVisibility(View.VISIBLE);
		layout_iv_map.setVisibility(View.VISIBLE);
		layout_tvs_news.setVisibility(View.VISIBLE);
		layout_iv_news.setVisibility(View.VISIBLE);
	}

	
	
	private void setBarVisible() {
		progressBarMain.setVisibility(View.VISIBLE);
		progressBarMap.setVisibility(View.VISIBLE);
		progressBarNews.setVisibility(View.VISIBLE);
	}
	
	private void setMainAdapter() {
		adapter = new MainAdapter(context,mainLayouts);
		lvMain.setAdapter(adapter);
	}

}
