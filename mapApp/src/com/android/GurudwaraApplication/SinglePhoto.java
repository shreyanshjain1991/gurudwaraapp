package com.android.GurudwaraApplication;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.gurudwara.imagepager.JazzyViewPager.TransitionEffect;
import com.gurudwara.imagepager.OutlineContainer;
import com.gurudwara.utils.Constants;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

public class SinglePhoto extends SherlockActivity{

	int index;
	int previousPosition=0;
	com.gurudwara.imagepager.JazzyViewPager mJazzy;
	MainAdapter mAdapter;
	ActionBar singlePhotoBar;
	private ArrayList<String> photoList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.jazzy_view_pager_main);

		
		index = getIntent().getExtras().getInt("index");
		photoList=getIntent().getStringArrayListExtra("photoList");
		singlePhotoBar=getSupportActionBar();
		setupJazziness(TransitionEffect.CubeOut);
		singlePhotoBarFormatting();


	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}


	public void singlePhotoBarFormatting() {
		singlePhotoBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#ee5c0c")));
		singlePhotoBar.setTitle("Photo View");
		singlePhotoBar.setDisplayShowHomeEnabled(false);
		singlePhotoBar.setDisplayHomeAsUpEnabled(true);
	}


	private void setupJazziness(TransitionEffect effect) {
		mJazzy = (com.gurudwara.imagepager.JazzyViewPager)findViewById(R.id.jazzy_pager);
		mJazzy.setTransitionEffect(effect);
		mAdapter=new MainAdapter(SinglePhoto.this, photoList);
		mJazzy.setAdapter(mAdapter);
		mJazzy.setPageMargin(30);
		mJazzy.setCurrentItem(index);
	}

	private class MainAdapter extends PagerAdapter
	{

		ArrayList<String> imageList;
		Context context;

		MainAdapter(Context context, ArrayList<String> list)
		{	
			imageList=list;
			this.context = context;
		}

		@Override
		public int getCount() {
			return imageList.size();
		}

		@Override
		public boolean isViewFromObject(View view, Object obj) {
			if (view instanceof OutlineContainer) {
				return ((OutlineContainer) view).getChildAt(0) == obj;
			} else {
				return view == obj;
			}
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView(mJazzy.findViewFromObject(position));
		}


		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			ImageView image = new ImageView(context);
			setImageToImageView(position, image,container );
			return image;
		}

		private void setImageToImageView(int pos, ImageView img,ViewGroup c)
		{
			UrlImageViewHelper.setUrlDrawable(img, imageList.get(pos));
			c.addView(img, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			mJazzy.setObjectForPosition(img, pos);
		}

	}




}
