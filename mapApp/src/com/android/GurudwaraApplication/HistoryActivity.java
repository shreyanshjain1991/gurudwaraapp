package com.android.GurudwaraApplication;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.gurudwara.bean.HistoryBean;
import com.gurudwara.parser.JsonParser;
import com.gurudwara.utils.ConnectionDetector;
import com.gurudwara.utils.Constants;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;


public class HistoryActivity extends SherlockActivity {

	private ActionBar historyBar;
	private Typeface tf;
	private ProgressBar progressBarHistory;
	private TextView historyGurudwaraName;
	private TextView historyGurudwaraLocation;
	private ImageView historyImage;
	private TextView historyDetails;
	private TextView noHistory;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.history);
		initialiseVariable();
		historyBarFormatting();
	}

	//action bar formatting
	private void historyBarFormatting() {
		historyBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ee5c0c")));
		historyBar.setTitle("HISTORY");
		historyBar.setDisplayShowHomeEnabled(false);
		historyBar.setDisplayHomeAsUpEnabled(true);
	}

	//initialises variables.
	private void initialiseVariable() {
		historyBar=getSupportActionBar();
		historyDetails=(TextView)findViewById(R.id.historyDetails);
		if(new ConnectionDetector(HistoryActivity.this).isConnectingToInternet()){
		getDataFromUrl(Constants.historyUrl);
	}else{
		Toast.makeText(HistoryActivity.this, HistoryActivity.this.getResources().getString(R.string.please_connect_to_internet), Toast.LENGTH_LONG).show();
	}
		progressBarHistory=(ProgressBar)findViewById(R.id.progressBarHistory);
		historyGurudwaraName=(TextView)findViewById(R.id.historyGurudwaraName);
		historyGurudwaraLocation=(TextView)findViewById(R.id.historyGurudwaraLocation);
		historyImage=(ImageView)findViewById(R.id.historyImage);
		historyDetails=(TextView)findViewById(R.id.historyDetails);
		noHistory=(TextView)findViewById(R.id.noHistory);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return false;
	}



	public void getDataFromUrl(String webUrl){
		RequestParams params = new RequestParams();
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(webUrl,params, new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				Log.d("onFailure","in on failure "+arg1);
				super.onFailure(arg0, arg1);
			}

			@Override
			public void onFinish() {
				Log.d("onFinish","in on finish ");
				super.onFinish();
			}

			@Override
			public void onStart() {
				progressBarHistory.setVisibility(View.VISIBLE);
				super.onStart();
			}

			@Override
			public void onSuccess(String arg0) {
				Log.d("onSuccess","the string : "+arg0);
				HistoryBean bean =  JsonParser.parseJsonRespHistory(arg0);
				if (bean!=null) {
					progressBarHistory.setVisibility(View.GONE);
					historyGurudwaraName.setVisibility(View.VISIBLE);
					historyGurudwaraName.setText(bean.getHistoryGurudwaraName());
					historyGurudwaraLocation.setVisibility(View.VISIBLE);
					historyGurudwaraLocation.setText(bean.getHistoryGurudwaraAddress());
					historyImage.setVisibility(View.VISIBLE);
					UrlImageViewHelper.setUrlDrawable(historyImage, bean.getHistoryImageUrl());
					tf = Typeface.createFromAsset(getAssets(),"anmolunib.ttf");
					historyDetails.setTypeface(tf);     
					historyDetails.setText(Html.fromHtml(bean.getHistoryDetails().toString()).toString());
					historyDetails.setVisibility(View.VISIBLE);
						}
				else
				{
					progressBarHistory.setVisibility(View.GONE);
					noHistory.setVisibility(View.VISIBLE);
				}
				super.onSuccess(arg0);
			}

		});
	}

}
