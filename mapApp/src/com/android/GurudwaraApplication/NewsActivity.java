package com.android.GurudwaraApplication;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.gurudwara.adapter.EventListAdapter;
import com.gurudwara.adapter.NewsListAdapter;
import com.gurudwara.bean.EventListBean;
import com.gurudwara.bean.NewsListBean;
import com.gurudwara.parser.JsonParser;
import com.gurudwara.utils.ConnectionDetector;
import com.gurudwara.utils.Constants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class NewsActivity extends SherlockActivity{

	private ListView newsList;
	private Context context;
	private ActionBar newsBar;
	private ArrayList<NewsListBean> newsListBean;
	private TextView newsHeading;
	private TextView newsDescription;
	private Typeface tf;
	private ProgressBar progressBarNews;
	private int screenHeight;
	private ImageView newsImageButton;
	private TextView noNews;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news_list);
		context=NewsActivity.this;
		initialiseVariable();
		newsBarFormatting();
		if(new ConnectionDetector(NewsActivity.this).isConnectingToInternet()){
			getDataFromUrl(Constants.newsUrl);
		}else{
			Toast.makeText(context, context.getResources().getString(R.string.please_connect_to_internet), Toast.LENGTH_LONG).show();
		}
			
	}



	public void initialiseVariable() {

		noNews=(TextView)findViewById(R.id.noNews);
		newsList = (ListView) findViewById(R.id.lvNews);
		newsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
				Log.e("","on item selected listener");
				Intent i=new Intent(context,NewsDescription.class);
				i.putExtra("newsId", newsListBean.get(position).getNewsId());
				i.putExtra("newsName", newsListBean.get(position).getNewsHeading());
				i.putExtra("newsDescription", newsListBean.get(position).getNewsDescription());
				i.putExtra("newsImage", newsListBean.get(position).getUrlNewsImage());
				startActivity(i);
			}
		});


		newsBar = getSupportActionBar();
		progressBarNews=(ProgressBar)findViewById(R.id.progressBarNews);



	}

	public void newsBarFormatting() {
		newsBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ee5c0c")));
		newsBar.setTitle("NEWS");
		newsBar.setDisplayShowHomeEnabled(false);
		newsBar.setDisplayHomeAsUpEnabled(true);
	}

	public void getDataFromUrl(String webUrl){
		RequestParams params = new RequestParams();
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(webUrl,params, new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				Log.d("onFailure","in on failure "+arg1);
				super.onFailure(arg0, arg1);
			}

			@Override
			public void onFinish() {
				Log.d("onFinish","in on finish ");
				super.onFinish();
			}

			@Override
			public void onStart() {
				Log.d("onStart","in on start");
				progressBarNews.setVisibility(View.VISIBLE);
				super.onStart();
			}

			@Override
			public void onSuccess(String arg0) {
				Log.d("onSuccess","the string : "+arg0);
				newsListBean =  JsonParser.parseJsonRespNews(arg0);
				if (newsListBean.size()!=0) {
					progressBarNews.setVisibility(View.GONE);
					newsList.setVisibility(View.VISIBLE);
					NewsListAdapter elAdapter = new NewsListAdapter(context,newsListBean);
					newsList.setAdapter(elAdapter);
				}
				else
				{
					progressBarNews.setVisibility(View.GONE);
					noNews.setVisibility(View.VISIBLE);
				}
				super.onSuccess(arg0);
			}

		});
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	
}
