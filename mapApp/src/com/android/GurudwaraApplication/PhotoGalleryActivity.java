package com.android.GurudwaraApplication;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.gurudwara.adapter.AdapterPhotoGallery;
import com.gurudwara.bean.PhotoGalleryBean;
import com.gurudwara.parser.JsonParser;
import com.gurudwara.utils.ConnectionDetector;
import com.gurudwara.utils.Constants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class PhotoGalleryActivity extends SherlockActivity{


	private ActionBar photoGalleryBar;
	private GridView gvMain;
	private ProgressBar progressBarGallery;
	private Context context;
	private ArrayList<String> imageList;
	private TextView noPhoto;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gallery_layout);
		initialiseVariable();
		photoGalleryBarFormatting();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	@Override
	protected void onResume() {
		if(new ConnectionDetector(PhotoGalleryActivity.this).isConnectingToInternet()){
		getDataFromUrl(Constants.photoGalleryUrl);
	}else{
		Toast.makeText(PhotoGalleryActivity.this, PhotoGalleryActivity.this.getResources().getString(R.string.please_connect_to_internet), Toast.LENGTH_LONG).show();
	}
		super.onResume();
	}


	private void initialiseVariable() {

		imageList=new ArrayList<String>();
		context=this;
		gvMain=(GridView)findViewById(R.id.gridView1);
		//gvMain.setColumnWidth(UIScheme.cellSize);
		progressBarGallery=(ProgressBar)findViewById(R.id.progressBarGallery);
		noPhoto=(TextView)findViewById(R.id.noPhoto);
		
		gvMain.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
				try {
					Intent intent=new Intent(PhotoGalleryActivity.this,SinglePhoto.class);
					intent.putExtra("index", position);
					intent.putStringArrayListExtra("photoList", imageList);
					startActivity(intent);
				} catch (Exception e) {
					Toast.makeText(PhotoGalleryActivity.this, "Error : "+e.toString(), 3000).show();
					e.printStackTrace();
				}

			}
		});

		photoGalleryBar=getSupportActionBar();
	}



	private void photoGalleryBarFormatting() {

		photoGalleryBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ee5c0c")));
		photoGalleryBar.setTitle("PHOTO GALLERY");
		photoGalleryBar.setDisplayShowHomeEnabled(false);
		photoGalleryBar.setDisplayHomeAsUpEnabled(true);
	}

	//id=11
	//pending vertical spacing between the photos.

	public void getDataFromUrl(String webUrl){
		RequestParams params = new RequestParams();
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(webUrl,params, new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				Log.d("onFailure","in on failure "+arg1);
				super.onFailure(arg0, arg1);
			}

			@Override
			public void onFinish() {
				Log.d("onFinish","in on finish ");
				super.onFinish();
			}

			@Override
			public void onStart() {
				progressBarGallery.setVisibility(View.VISIBLE);
				super.onStart();
			}

			@Override
			public void onSuccess(String arg0) {
				Log.d("onSuccess","the string : "+arg0);

				imageList.clear();
				imageList = JsonParser.parseJsonRespGallery(arg0);
				if (imageList.size()!=0) {
					progressBarGallery.setVisibility(View.GONE);
					AdapterPhotoGallery gd=new AdapterPhotoGallery(context, imageList);
					gvMain.setAdapter(gd);
					gvMain.setVisibility(View.VISIBLE);
				}
				else
				{
					progressBarGallery.setVisibility(View.GONE);
					noPhoto.setVisibility(View.VISIBLE);
				}
				super.onSuccess(arg0);
			}

		});
	}

}
