package com.android.GurudwaraApplication;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.gurudwara.utils.Helper;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

public class NewsDescription extends SherlockActivity{

	private TextView newsHeading;
	private TextView newsDescription;
	private ActionBar newsDescriptionBar;
	private Bundle extras;
	private ImageView newsDesImage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news_des_activity);
		extras=getIntent().getExtras();
		initialiseVariable();
		newsDescriptionBarFormatting();

	}


	private void initialiseVariable() {

		newsHeading=(TextView)findViewById(R.id.news_des_heading);
		newsDescription=(TextView)findViewById(R.id.news_details);
		newsDesImage=(ImageView)findViewById(R.id.news_des_image);
		newsDescriptionBar=getSupportActionBar();
		if(extras!=null)
		{
			Helper.setFont(this,newsHeading);
			Helper.setFont(this,newsDescription);
			newsHeading.setText(extras.getString("newsName"));
			newsDescription.setText(Html.fromHtml(extras.getString("newsDescription")).toString());
			UrlImageViewHelper.setUrlDrawable(newsDesImage, extras.getString("newsImage"));
			newsDesImage.setScaleType(ScaleType.FIT_XY);
		}
	}

	//action bar formatting
	private void newsDescriptionBarFormatting() {
		newsDescriptionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ee5c0c")));
		newsDescriptionBar.setTitle("NEWS DESCRIPTION");
		newsDescriptionBar.setDisplayShowHomeEnabled(false);
		newsDescriptionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return false;
	}




	
}
