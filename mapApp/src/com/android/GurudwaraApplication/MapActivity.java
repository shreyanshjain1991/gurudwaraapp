package com.android.GurudwaraApplication;




import java.util.ArrayList;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gurudwara.bean.GurudwaraNameBean;
import com.gurudwara.bean.HistoryBean;
import com.gurudwara.bean.MapBean;
import com.gurudwara.parser.JsonParser;
import com.gurudwara.utils.Constants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class MapActivity extends SherlockFragmentActivity {

	SupportMapFragment mapFragment;
	GoogleMap map;
	private Double lat;
	private Double lon;
	private String gurudwaraTitle;
	private String gurudwaraSubTitle;
	private ActionBar mapBar;
	private MapBean mapBean;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_activity);
		Bundle extras=getIntent().getExtras();
		
		//I am getting the data from the main activity.
		if(extras!=null)
		{
			gurudwaraTitle=extras.getString("gurudwaraName");
		gurudwaraSubTitle=extras.getString("gurudwaraLocation");
		Log.e("","lat: "+extras.getString("lat"));
		lat=Double.parseDouble(extras.getString("lat"));
		
		lon=Double.parseDouble(extras.getString("lon"));
		}
		else
			Log.e("","bundle is null");
		
		mapFragment=(SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
		map=mapFragment.getMap();
		initialiseVariable();
		mapBarFormatting();
		if(map!=null)
		{
			addMarkerInMap();
			setupMapSettings();
		}
		else
		{
			Log.d("Map Null","Map Null");
		}
	}

	public void mapBarFormatting() {
		mapBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#ee5c0c")));
		mapBar.setTitle("MAP");
		mapBar.setDisplayShowHomeEnabled(false);
		mapBar.setDisplayHomeAsUpEnabled(true);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}


	


	

	private void setupMapSettings() {
		map.setTrafficEnabled(true);
		map.getUiSettings().setCompassEnabled(true);
		map.getUiSettings().setZoomGesturesEnabled(true);
		map.getUiSettings().setZoomControlsEnabled(true);
	}

	private void initialiseVariable()
	{
		mapBar=getSupportActionBar();
		//getDataFromUrl(Constants.nameUrl);
	}

	

	private void addMarkerInMap() {
			addCustomMarkers();	
		map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 10));
	}

	private void addCustomMarkers() {
		map.addMarker(new MarkerOptions().position(new LatLng(lat,lon)).title(gurudwaraTitle).snippet(gurudwaraSubTitle));

	}

//	public void getDataFromUrl(String webUrl)
//	{
//		Log.e("", "Entered the map activity and getDataFromUrl");
//		RequestParams params= new RequestParams();
//		AsyncHttpClient client=new AsyncHttpClient();
//		client.get(webUrl, params,new AsyncHttpResponseHandler()
//		{
//
//			@Override
//			public void onFailure(Throwable arg0, String arg1) {
//				Log.e("", "Entered the map activity/getDataFromUrl/onFailure");
//				super.onFailure(arg0, arg1);
//			}
//
//			@Override
//			public void onFinish() {
//				Log.e("", "Entered the map activity/getDataFromUrl/onFinish");
//				super.onFinish();
//				
//			}
//
//			@Override
//			public void onStart() {
//				Log.e("", "Entered the map activity/getDataFromUrl/onStart");
//				super.onStart();
//				
//			}
//
//			@Override
//			public void onSuccess(String arg0) {
//				Log.e("", "Entered the map activity/getDataFromUrl/onSuccess");
//				GurudwaraNameBean bean =  JsonParser.parseJsonRespMain(arg0);
//				if (bean!=null) {
//					lat=Double.parseDouble(bean.getGurudwaraLat());
//					lon=Double.parseDouble(bean.getGurudwaraLon());
//					gurudwaraTitle=bean.getGurudwaraName();
//					gurudwaraSubTitle=bean.getGurudwaraAddress();
//					Log.e("", "Value of latitude: "+lat);
//					Log.e("", "Value of longitude: "+lon);
//				}
//				Log.e("", "Exited the map activity/getDataFromUrl/onSuccess");
//				super.onSuccess(arg0);
//			}
//
//		}
//				);
//		Log.e("", "Exited the map activity and getDataFromUrl");
//	}
}