package com.android.GurudwaraApplication;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.gurudwara.adapter.EventListAdapter;
import com.gurudwara.bean.EventListBean;
import com.gurudwara.parser.JsonParser;
import com.gurudwara.utils.ConnectionDetector;
import com.gurudwara.utils.Constants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class EventListActivity extends SherlockActivity {

	private ListView eventList;
	private Context context;
	private ActionBar eventBar;
	private ArrayList<EventListBean> eventListBean;
	private TextView eventHeading;
	private TextView eventDescription;
	private Typeface tf;
	private ProgressBar progressBarEvent;
	private int screenHeight;
	private ImageView eventImageButton;
	private TextView noEvent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_list);
		context=EventListActivity.this;
		initialiseVariable();
		eventBarFormatting();
		if(new ConnectionDetector(EventListActivity.this).isConnectingToInternet()){
			getDataFromUrl(Constants.eventUrl);
		}else{
			Toast.makeText(context, EventListActivity.this.getResources().getString(R.string.please_connect_to_internet), Toast.LENGTH_LONG).show();
		}
			
	}



	public void initialiseVariable() {

		noEvent=(TextView)findViewById(R.id.noEvent);
		eventList = (ListView) findViewById(R.id.lvEvent);
		eventList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
				Log.e("","on item selected listener");
				Intent i=new Intent(EventListActivity.this,EventDescription.class);
				i.putExtra("eventName", eventListBean.get(position).getEventHeading());
				i.putExtra("eventDescription", eventListBean.get(position).getEventDescription());
				i.putExtra("eventImage", eventListBean.get(position).getUrlEventImage());
				startActivity(i);
			}
		});


		eventImageButton=(ImageView)findViewById(R.id.iv_join_event);
		eventBar = getSupportActionBar();
		progressBarEvent=(ProgressBar)findViewById(R.id.progressBarEvent);



	}

	public void eventBarFormatting() {
		eventBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ee5c0c")));
		eventBar.setTitle("EVENTS");
		eventBar.setDisplayShowHomeEnabled(false);
		eventBar.setDisplayHomeAsUpEnabled(true);
	}

	public void getDataFromUrl(String webUrl){
		RequestParams params = new RequestParams();
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(webUrl,params, new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				Log.d("onFailure","in on failure "+arg1);
				super.onFailure(arg0, arg1);
			}

			@Override
			public void onFinish() {
				Log.d("onFinish","in on finish ");
				super.onFinish();
			}

			@Override
			public void onStart() {
				Log.d("onStart","in on start");
				progressBarEvent.setVisibility(View.VISIBLE);
				super.onStart();
			}

			@Override
			public void onSuccess(String arg0) {
				Log.d("onSuccess","the string : "+arg0);
				eventListBean =  JsonParser.parseJsonRespEvent(arg0);
				if (eventListBean.size()!=0) {
					progressBarEvent.setVisibility(View.GONE);
					eventList.setVisibility(View.VISIBLE);
					EventListAdapter elAdapter = new EventListAdapter(context,eventListBean);
					eventList.setAdapter(elAdapter);
				}
				else
				{
					progressBarEvent.setVisibility(View.GONE);
					noEvent.setVisibility(View.VISIBLE);
				}
				super.onSuccess(arg0);
			}

		});
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}
}
