package com.android.GurudwaraApplication;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.gurudwara.bean.EventListBean;
import com.gurudwara.bean.HistoryBean;
import com.gurudwara.parser.JsonParser;
import com.gurudwara.utils.Helper;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class EventDescription extends SherlockActivity{

	private TextView eventHeading;
	private TextView eventDescription;
	private ActionBar eventDescriptionBar;
	private Bundle extras;
	private ImageView eventDesImage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_des_activity);
		extras=getIntent().getExtras();
		initialiseVariable();
		eventDescriptionBarFormatting();

	}


	private void initialiseVariable() {

		eventHeading=(TextView)findViewById(R.id.event_des_heading);
		eventDescription=(TextView)findViewById(R.id.event_details);
		eventDesImage=(ImageView)findViewById(R.id.event_des_image);
		eventDescriptionBar=getSupportActionBar();
		if(extras!=null)
		{
			Helper.setFont(this,eventHeading);
			Helper.setFont(this,eventDescription);
			eventHeading.setText(extras.getString("eventName"));
			eventDescription.setText(Html.fromHtml(extras.getString("eventDescription")).toString());
			UrlImageViewHelper.setUrlDrawable(eventDesImage, extras.getString("eventImage"));
			eventDesImage.setScaleType(ScaleType.FIT_XY);
		}
	}

	//action bar formatting
	private void eventDescriptionBarFormatting() {
		eventDescriptionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ee5c0c")));
		eventDescriptionBar.setTitle("EVENT DESCRIPTION");
		eventDescriptionBar.setDisplayShowHomeEnabled(false);
		eventDescriptionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return false;
	}




}
